packages needed:
sudo apt-get install gcc-avr avr-libc avrdude

I used the sparkfune AVR Pocket programmer, a USBtinyISP clone (http://ladyada.net/make/usbtinyisp/)
it uses libusb, not a serial port
to be able to use it a non-root part of the dialout group (on ubuntu distributions)
echo 'SUBSYSTEM=="usb", ATTRS{idVendor}=="1781", ATTR{idProduct}=="0c9f", MODE="0660", GROUP="dialout"' | sudo tee /etc/udev/rules.d/99-avr.rules
sudo service udev reload

