require 'rake/clean'

# ==============
# important info
# ==============

target = "ir-cock-grenade"
version = IO.read("version").chomp
date = Time.now.strftime("%Y-%m-%d")
revision = `git log --pretty=oneline "#{target}.sch" | wc -l`.chomp.to_i
# symbol library for gschem
LIB = "lib/symbols/"
# gEDA scheme
GEDA_SCHEME_DIRS=["/usr/share/gEDA/scheme","/usr/local/gEDA/scheme"].collect{|path| File.directory?(path) ? path : nil}.compact
unless GEDA_SCHEME_DIRS.size==0 then
  GEDA_SCHEME_DIR=GEDA_SCHEME_DIRS[0]
else
  GEDA_SCHEME_DIR=nil
  puts "warning: could not find gEDA scheme directory. can not print schematic"
end


# schema
sch = "#{target}.sch"
# schema with version
vsch = "#{target}_v#{version}.#{revision.to_s.rjust(3,'0')}.sch"

# =========
# the tasks
# =========

task :default => [:version,:print,:pdf]

desc "set version in schema"
task :version => vsch
CLEAN.include(vsch)
CLOBBER.include("#{target}_*.sch")

desc "print schema (into ps)"
task :print => "#{target}.ps"
CLEAN.include("#{target}.ps")

desc "get printed schema in pdf"
task :pdf => "#{target}.pdf"
CLEAN.include("#{target}.pdf")

# ===============
# file processing
# ===============

file vsch => sch do
  sh "cp #{sch} #{vsch}"
  # on \ is to prevent ruby interpreting it, th other is for sed
  # the version
  sh "sed -i 's/\\(version=\\)\\$Version\\$/\\1#{version}/' #{vsch}"
  # the date
  sh "sed -i 's/\\(date=\\)\\$Date\\$/\\1#{date}/' #{vsch}"
  # the revision
  sh "sed -i 's/\\(revision=\\)\\$Revision\\$/\\1#{revision}/' #{vsch}"
end

file "#{target}.ps" => vsch do
  if GEDA_SCHEME_DIR then
    sh "gschem -p -o #{target}.ps -s #{GEDA_SCHEME_DIR}/print.scm #{vsch} > /dev/null 2>&1"
  else
    puts "can not print schematic. gEDA scheme directory missing"
  end
end

file "#{target}.pdf" => "#{target}.ps" do
  sh "ps2pdf -sPAPERSIZE=a4 #{target}.ps"
end
