#!/usr/bin/env ruby
# encoding: UTF-8
# tested with ruby 1.9.1 and 1.9.3
# decode milestag II message recorded by ir.rb

UNIT = 21.3333 # ir burst unit, in µs

# verify command parameters
raise "provide a IR record file to parse" if ARGV.size!=1
# read file
file = File.open(ARGV[0],"r")

# parse ticks in IR packets
ir_packets = []
packet = []
while data = file.read(2) do
  # one IR packet
  burst = data.unpack('n')[0]
  if burst==0xffff then
    # create next packet
    packet << 1 # trailing space
    ir_packets << packet
    packet = []
  else
    # decode tick
    burst *= UNIT
    # the number of ticks (a tick = 600µs)
    tick = (burst/600.0).round
    packet << tick
  end
end
puts "got #{ir_packets.size} IR packets"
puts ir_packets*" "

# decode ticks in bits
bit_packets = []
ir_packets.each do |ir_packet|
  bit_packet = []
  header = false
  space = false
  ir_packet.each do |ir|
    # wait for header
    if ir==4 then
      if bit_packet.size>0 then
        bit_packets << bit_packet
        bit_packet = []
      end
      header = true
      space = true
    else
      next unless header
      if space then
        space = false
        next
      else
        if ir==1 or ir==2 then
          bit_packet<<(ir-1)
          space = true
        else
          puts "broken burst"
          header = false
        end
      end
    end
  end
  bit_packets << bit_packet
end
puts "got #{bit_packets.size} bit packets"
puts bit_packets*" "

# parse bit packet
bit_packets.each do |bit_packet|
  if bit_packet[0]==0 then
    # shot packet
    if bit_packet.size!=14 then
      puts "broken shot packet"
    else
      player = (bit_packet[1,7]*"").to_i(2)
      team = (bit_packet[8,2]*"").to_i(2)
      damage = (bit_packet[10,4]*"").to_i(2)
      puts "shot: player=#{player}, team=#{team}, damage=#{damage}"
    end
  else
    # control packet
    if bit_packet.size%8!=0 then
      puts "broken control packet"
    else
      data = (bit_packet*"").to_i(2).to_s(16).rjust(bit_packet.size/4,"0")
      puts "control packet: 0x#{data}"
    end
  end
end
