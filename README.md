LaserTag infra-red greande.
based on the MilesTag v2 protocol

usb ir toy
==========

script to record and decode infra-red signal using the Dangerous Prototypes USB IR toy board.
I had to replace the 38 kHz demodulator with a 56 kHz demodulator.

ir.rb
-----

script to use the USB IR toy.
can detect frequency, record transmission, play recordings, ...

decode.rb
---------

decode the MilesTag v2 recorded transmission

avr
===

firmware for the IR grenade.
based on the TV-B-Gone firmware

